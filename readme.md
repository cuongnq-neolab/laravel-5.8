## Install docker CE, docker compose
<ul>
    <li>Docker for mac: https://docs.docker.com/docker-for-mac/install/</li>
    <li>Docker for ubuntu: https://docs.docker.com/install/, https://docs.docker.com/compose/install/</li>
</ul>

## Set Permission for Docker
<ul>
<li>
1. Create the docker group.

```
$ sudo groupadd docker
```
</li>
<li>
2. Add your user to the docker group.

```
$ sudo usermod -aG docker $USER
```
</li>
<li>
3. Logout and login again and run for test.

```
$ docker run hello-world
```
</li>
</ul>


## Install source code
```
$ git clone https://gitlab.com/cuongnq-neolab/laravel-5.8.git myproject
$ cd ./myproject
$ docker run --rm -v $(pwd):/app composer install
$ sudo chown -R $USER:$USER ../myproject
```

## Run docker & create mysql user
```
$ docker-compose up -d
$ docker-compose exec db bash
# mysql -u root -p
> mysql show databases;
> mysql use laravel;
> mysql GRANT ALL ON laravel.* TO 'laraveluser'@'%' IDENTIFIED BY 'your_password';
> mysql FLUSH PRIVILEGES;
> mysql EXIT;
# exit;
```

## Migrating Data
```
$ docker-compose exec app php artisan migrate
```

## Run

Open http://localhost or http://127.0.0.1
